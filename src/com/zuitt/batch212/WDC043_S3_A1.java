package com.zuitt.batch212;

import java.util.Scanner;

public class WDC043_S3_A1 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Input an integer whose factorial will be computed");
        int num = in.nextInt();
        int answer = 1;

        if (num >= 1){
            while (num>0){
                answer *= num;
                num--;
            }
            System.out.println("The factorial of " + num + " is " + answer);
        }
        else {
            System.out.println("Please input a valid number.");
        }


        try {
            System.out.println("Input an integer whose factorial will be computed");
            int number = in.nextInt();
            int answer2 = 1;

            if(number > 0){
                for (int i = 1; i <= number; i++) {
                    answer2 = answer2 * i;
                }
                System.out.println("The factorial of " + number + " is " + answer2);
            }
            else {
                System.out.println("Please input a valid number.");
            }
        }
        catch (Exception e) {
            System.out.println("Please input a number.");
        }

    }

}
